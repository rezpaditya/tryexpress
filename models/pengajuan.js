"use strict";

module.exports = function(sequelize, DataTypes) {
  var Pengajuan = sequelize.define("Pengajuan", {
    kode_aset: DataTypes.STRING,
    kode_booking: DataTypes.STRING,
    status_booking: DataTypes.STRING,
    start_date: DataTypes.STRING,
    end_date: DataTypes.STRING,
    description: DataTypes.STRING,
    created_by: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    jenis_aset: DataTypes.STRING,
    alamat_aset: DataTypes.STRING,
    luas_aset: DataTypes.FLOAT
  });

  Pengajuan.associate = function (models) {
    Pengajuan.belongsTo(models.User, {foreignKey: 'created_by' , as: 'User'});
  };
  
  return Pengajuan;
};