"use strict";

module.exports = function (sequelize, DataTypes) {
    var User = sequelize.define("User", {
        name: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        nip: DataTypes.STRING,
        business_unit: DataTypes.STRING,
        sub_business_unit: DataTypes.STRING,
        mobile_phone: DataTypes.STRING,
        jenjang: DataTypes.STRING,
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE
    });

    User.associate = function (models) {
        User.hasMany(models.RoleUser, {foreignKey: 'user_id', as: 'RoleUser'});
        User.hasMany(models.Pengajuan, {foreignKey: 'created_by', as: 'PengajuanUser'});
    };

    return User;
};