"use strict";

module.exports = function(sequelize, DataTypes) {
  var RoleUser = sequelize.define("RoleUser", {
    user_id: DataTypes.INTEGER,
    role_id: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  });

  RoleUser.associate = function (models) {
    RoleUser.belongsTo(models.User, {foreignKey: 'user_id', as : 'User'});
  };

  RoleUser.associate = function (models) {
    RoleUser.belongsTo(models.Role, {foreignKey: 'role_id', as : 'Role'});
  };
  
  return RoleUser;
};