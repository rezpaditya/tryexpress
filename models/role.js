"use strict";

module.exports = function(sequelize, DataTypes) {
  var Role = sequelize.define("Role", {
    role_name: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  });
  
  Role.associate = function (models) {
    Role.hasMany(models.RoleUser, {foreignKey: 'role_id', as : 'RoleUser'});
  };

  return Role;
};