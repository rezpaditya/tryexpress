-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.0.20-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for asset_property
CREATE DATABASE IF NOT EXISTS `asset_property` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `asset_property`;

-- Dumping structure for table asset_property.pengajuans
CREATE TABLE IF NOT EXISTS `pengajuans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_aset` varchar(50) NOT NULL DEFAULT '0',
  `kode_booking` varchar(50) NOT NULL DEFAULT '0',
  `status_booking` varchar(50) NOT NULL DEFAULT '0',
  `start_date` varchar(50) NOT NULL DEFAULT '0',
  `end_date` varchar(50) NOT NULL DEFAULT '0',
  `description` varchar(50) NOT NULL DEFAULT '0',
  `created_by` varchar(50) NOT NULL DEFAULT '0',
  `createdAt` varchar(50) NOT NULL DEFAULT '0',
  `updatedAt` varchar(50) NOT NULL DEFAULT '0',
  `jenis_aset` varchar(50) DEFAULT NULL,
  `alamat_aset` varchar(50) DEFAULT NULL,
  `luas_aset` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table asset_property.pengajuans: ~2 rows (approximately)
/*!40000 ALTER TABLE `pengajuans` DISABLE KEYS */;
-- INSERT INTO `pengajuans` (`id`, `kode_aset`, `kode_booking`, `status_booking`, `start_date`, `end_date`, `description`, `created_by`, `createdAt`, `updatedAt`, `jenis_aset`, `alamat_aset`, `luas_aset`) VALUES
	-- (11, '199-W-540100052-001-0001', 'KD-BK-1510546873962', 'APPROVED', '12/01/2017', '12/02/2017', 'joss', '0', '2017-11-13 04:21:24', '2017-11-13 06:43:56', NULL, NULL, NULL),
	-- (13, '199-0001', 'KD-BK-1510883581579', 'CREATED', '11/17/2017', '11/18/2017', 'Pinjam', '0', '2017-11-17 01:53:13', '2017-11-17 01:53:13', 'TANAH', 'Jl. Trunojoyo Blok M-I no 135, Jakarta Selatan', 5000);
/*!40000 ALTER TABLE `pengajuans` ENABLE KEYS */;

-- Dumping structure for table asset_property.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) DEFAULT NULL,
  `createdAt` date DEFAULT NULL,
  `updatedAt` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table asset_property.roles: ~3 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
-- INSERT INTO `roles` (`id`, `role_name`, `createdAt`, `updatedAt`) VALUES
-- 	(1, 'ADMIN', '2017-11-13', '2017-11-13'),
-- 	(2, 'GUEST', '2017-11-15', '2017-11-15'),
-- 	(3, 'APPROVER', '2017-11-15', '2017-11-15');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table asset_property.roleusers
CREATE TABLE IF NOT EXISTS `roleusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `createdAt` date DEFAULT NULL,
  `updatedAt` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user_id`),
  KEY `role` (`role_id`),
  CONSTRAINT `role` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table asset_property.roleusers: ~2 rows (approximately)
/*!40000 ALTER TABLE `roleusers` DISABLE KEYS */;
-- INSERT INTO `roleusers` (`id`, `user_id`, `role_id`, `createdAt`, `updatedAt`) VALUES
-- 	(1, 1, 1, '2017-11-13', '2017-11-13'),
-- 	(2, 2, 2, '2017-11-17', '2017-11-17');
/*!40000 ALTER TABLE `roleusers` ENABLE KEYS */;

-- Dumping structure for table asset_property.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `createdAt` date DEFAULT NULL,
  `updatedAt` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table asset_property.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
-- INSERT INTO `users` (`id`, `name`, `email`, `password`, `createdAt`, `updatedAt`) VALUES
-- 	(1, 'Admin', 'admin@pln.co.id', '202cb962ac59075b964b07152d234b70', '2017-11-13', '2017-11-13'),
-- 	(2, 'Guest', 'guest@pln.co.id', '202cb962ac59075b964b07152d234b70', '2017-11-17', '2017-11-17');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
