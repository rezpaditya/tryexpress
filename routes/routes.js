var express = require('express');
var router = express.Router();
// var bodyParser = require('body-parser');
// var urlencodedParser = bodyParser.urlencoded({ extended: false });
var models  = require('../models');
//import controller
var assetController = require('../controller/assetController');
var authController = require('../controller/authController');
var userController = require('../controller/userController');

function isAuthenticated(req, res, next) {
  if (req.session.auth){
    return next();
  }else{
    res.render('login.ejs');
  }
}

/* GET home page. */
router.get('/', isAuthenticated, function(req, res, next) {
  res.redirect('/asset');
})

.get('/asset', isAuthenticated, assetController.index)

.get('/pengajuan', isAuthenticated, assetController.pengajuan)

.post('/form', isAuthenticated, assetController.form)

.post('/submission/add', isAuthenticated, assetController.save)

.get('/submission/delete/:id', isAuthenticated, assetController.delete)

.get('/submission/detail/:id', isAuthenticated, assetController.detail)

.get('/submission/approval/:id', isAuthenticated, assetController.approval)

.post('/submission/update', isAuthenticated, assetController.update)

.post('/asset/filter', isAuthenticated, assetController.filterAset)

.post('/submission/approve', isAuthenticated, assetController.approve)

.get('/login', authController.index)

.post('/login', authController.login)

.get('/logout', authController.logout)

.get('/user', userController.index)

.get('/form/user', userController.form)

.post('/user/add', userController.save)

.get('/user/delete/:id', userController.delete)

.get('/user/detail/:id', userController.detail)

.post('/user/update/', userController.update)

// .post('/aset/callApi/', assetController.callApi)

// ... ADD new route right here

.use(function(req, res, next){
  res.redirect('/pengajuan');
})

module.exports = router;
