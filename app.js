var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('cookie-session');
var bodyParser = require('body-parser');
var routes = require('./routes/routes');
var flash = require('connect-flash');
// var session = require('express-session');

var app = express();

app.engine('ejs', require('express-ejs-extend')); 
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use(session({
//   secret: 'keyboard cat',
//   resave: false,
//   saveUninitialized: true,
//   cookie: { secure: true }
// }));


// MySql connection
// var connection  = require('express-myconnection'),
// mysql = require('mysql');

// app.use(
//   connection(mysql,{
//     host     : 'localhost',
//     user     : 'root',
//     password : '',
//     database : 'asset_property',
//     debug    : false //set true if you wanna see debug logger
//   },'request')
// );

/* Using sessions */
app.use(session({secret: 'todotopsecret'}));
app.use(flash());

app.use(function(req, res, next) {
  res.locals.user = req.session.user;
  res.locals.isGuest = req.session.isGuest; 
  if(req.session.auth == null){
    req.session.auth = false;
  }
  next();
});

app.use('/', routes);

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   var err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  
  // render the error page
  res.status(err.status || 500);
  console.log('errorrrr!!! ' + err);
  res.send(err);

});

module.exports = app;
