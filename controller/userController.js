var models  = require('../models');
var md5 = require('md5');

exports.index = function(req, res){
  models.User.findAll().then(function(result){
    res.render('user.ejs', {data: result});
});
}

exports.form = function(req, res){
  res.render('form_user.ejs');
}

exports.save = function(req, res){
  models.User
  .create({
      nip: req.body.nip,
      name: req.body.nama,
      jenjang: req.body.jenjang,
      business_unit: req.body.business_unit,
      sub_business_uni: req.body.sub_business_uni,
      mobile_phone: req.body.mobile_phone,
      email: req.body.email,
      password: md5(req.body.password),
      createdAt: new Date(),
      updatedAt: new Date()
  
  })
  .then(function(result){
      var user_id = result.id;
      models.RoleUser
          .create({
              user_id : user_id,
              role_id : 2,
              createdAt: new Date(),
              updatedAt: new Date()
          }).then(function(result){
          res.redirect('/user');
      });
  });
}

exports.delete = function(req, res){
  models.User
  .destroy({
      where: {
          id: req.params.id
      }
  })
  .then(() => res.redirect('/user'));
}

exports.detail = function(req, res, next){
  models.User
  .findOne({
      where: {
          id: req.params.id
      }
  })
  .then((result) => {
      if(result != null){
          res.render('form_user.ejs', {data: result});
      }else{
          return next();
      }
  });
}

exports.update = function(req, res){
  const values = {
      nip: req.body.nip,
      name: req.body.nama,
      jenjang: req.body.jenjang,
      business_unit: req.body.business_unit,
      sub_business_unit: req.body.sub_business_unit,
      mobile_phone: req.body.mobile_phone,
      email: req.body.email
  };
   if(req.body.password != ""){
       values['password'] = md5(req.body.password);
   }
  
  const options = {
      where: {
          id: req.body.id
      }
  };
  
  console.log(values);
  
  models.User
  .update(values, options)
  .then(() => {
      models.User
      .findOne(options)
      .then((result) => {
          res.redirect('/user')
      });
  });
}