var models  = require('../models');
var md5 = require('md5');

exports.index = function(req, res){
  res.render('login.ejs');
}

function isGuest(user_id){
    return new Promise(function(resolve,reject){
        models.User
            .findOne({
                where: {
                    id: user_id
                },
                include:[{
                    model: models.RoleUser, as : 'RoleUser'
                }]
            })
            .then(function(result) {
                var arr_result = result.RoleUser;
                arr_result.forEach(element => {
                    models.RoleUser
                    .find({
                        where: {
                            id : element.id
                        },
                        include:[{
                            model: models.Role, as : 'Role'
                        }]
                    }).then(function(hasil){
                        var item = hasil.Role;
                        var isGuest = false;
                        if(item != null){
                            if(item.role_name == 'GUEST'){
                                isGuest = true;
                            }
                        }
                        resolve(isGuest);
                    });
            });
            });
    });

}

exports.login = function(req, res){
  var md5pass = md5(req.body.password);
  models.User
  .findOne({
    where: {
      email: req.body.username,
      password: md5pass
    }
  })
  .then((result) =>{
    if(result != null){
      req.session.user = result;
      req.session.auth = true;

      var isGuest_call = isGuest(result.id);
        isGuest_call.then(function(responses){
              res.locals.user = req.session.user;
              req.session.isGuest = responses;
              res.locals.isGuest = responses;
              res.redirect('/pengajuan');
        });
    }else{
      res.redirect('/login');
    }
  });
}

exports.logout = function(req, res){
  req.session = null;
  res.redirect('/login');
}