var models  = require('../models');
var url = 'http://api.datahub.dev/api';
var http = require('http');
var request = require('request');
var Promise = require('promise');

function callApiGet(path){
    return new Promise(function(resolve, reject) {
        request(url + '/' + path, function (error, response, body) {
            console.log ('CALL API : ' + url + '/' + path + " , METHOD :  GET");
            resolve(JSON.parse(body));
        });
    });
}

function callApiPost(path,body,headers){
    return new Promise(function(resolve, reject) {
        request.post({
            headers: headers,
            url:     url + '/' + path,
            body:    body
        }, function(error, response, body){
            console.log ('CALL API : ' + url + '/' + path + " , METHOD :  POST");
            resolve(JSON.parse(body));
        });
    });
}

exports.index = function(req, res){
    var bumn = "";
    var jenis = "";
    var provinsi = "";
    
    var bumn_call = callApiGet('bumn_data');
    var jenis_call = callApiGet('jenis_aset_data');
    var provinsi_call = callApiGet('province_data');
    
    new Promise.all([bumn_call,jenis_call,provinsi_call])
    .then(function(responses){
        bumn = responses[0];
        jenis = responses[1];
        provinsi = responses[2];
    }).then(function(){
        res.render('asset.ejs', {data_bumn: bumn,data_jenis: jenis,data_provinsi: provinsi,data_aset: null});
    });
}

exports.filterAset = function(req, res,next) {
    var req_bumn = req.body.bumn;
    var req_jenis = req.body.jenis;
    var req_provinsi = req.body.provinsi;
    var param  = '';
    if(req_bumn == 'all' && req_jenis == 'all' && req_provinsi == 'all'){
        param = 'data=all';
    }else{
        param = 'data=partial';
        param += (req_bumn > 0) ? '&bumn=' + req_bumn : '';
        param += (req_jenis > 0) ? '&jenis_aset=' + req_jenis : '';
        param += (req_provinsi > 0) ? '&provinsi=' + req_provinsi : '';
    }
    
    var bumn = "";
    var jenis = "";
    var provinsi = "";
    var data_aset = "";
    var login_data = null;
    // create request objects
    
    var bumn_call = callApiGet('bumn_data');
    var jenis_call = callApiGet('jenis_aset_data');
    var provinsi_call = callApiGet('province_data');
    var login = callApiPost('login',"email=dev@pln.co.id&password=123",{'content-type' : 'application/x-www-form-urlencoded'});
    
    new Promise.all([bumn_call,jenis_call,provinsi_call,login])
    .then(function(responses){
        bumn = responses[0];
        jenis = responses[1];
        provinsi = responses[2];
        login_data = responses[3];
    }).then(function(){
        var valid = true;
        if(login_data != null){
            if(login_data.status == 'success'){
                var token = login_data.api_key;
                callApiPost('aset?' + param,'',{
                    'content-type' : 'application/x-www-form-urlencoded',
                    'Authorization' : token
                }).then(function(response){
                    if(response.result != undefined)
                    data_aset = response.result;
                    res.render('asset.ejs', {data_bumn: bumn,data_jenis: jenis,data_provinsi: provinsi,data_aset : data_aset, status : 'ok'});
                });
            }else{
                valid = false;
            }
        }else{
            valid = false;
        }
        if(valid == false){
            res.render('asset.ejs', {data_bumn: bumn,data_jenis: jenis,data_provinsi: provinsi,data_aset : data_aset,status : 'Login fail'});
        }
    });
}

exports.pengajuan = function(req, res, next){
    models.Pengajuan.findAll().then(function(result){
        res.locals.success = req.flash('success');
        res.render('pengajuan.ejs', {data: result});
    });
}

exports.form = function(req, res){
    var aset = req.body.aset;
    aset = JSON.parse(aset);
    kode_booking = 'KD-BK-'+Date.now();
    res.render('form.ejs',{data_aset : aset, booking : kode_booking});
}

exports.save = function(req, res, next){
    models.Pengajuan
    .create({
        kode_aset: req.body.kode_aset,
        kode_booking: req.body.kode_booking,
        status_booking: req.body.status,
        description: req.body.description,
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        createdAt: new Date(),
        updatedAt: new Date(),
		jenis_aset: req.body.jenis_aset,
		alamat_aset : req.body.alamat_aset,
		luas_aset : req.body.luas_aset,
		
    })
    .then(function(result){
        req.flash('success','Data pengajuan berhasil disimpan.');
        res.redirect('/pengajuan');
    });
}

exports.delete = function(req, res, next){
    models.Pengajuan
    .destroy({
        where: {
            id: req.params.id
        }
    })
    .then(function(result){
        req.flash('success','Data pengajuan berhasil dihapus.');
        res.redirect('/pengajuan');
    });
}

exports.detail = function(req, res, next){
    models.Pengajuan
    .findOne({
        where: {
            id: req.params.id
        },
        include:[{
            model: models.User , as : 'User'
        }]
    })
    .then((result) => {
        console.log("PENGAJUAN " + JSON.stringify(result));
        if(result != null && result.status_booking == 'CREATED'){
            res.render('form.ejs', {data: result});
        }else{
            return next();
        }
    });
}

exports.update = function(req, res){
    const values = {
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        description: req.body.description,
        updatedAt: new Date(),
    };
    
    const options = {
        where: {
            id: req.body.id
        }
    };
    
    if(req.session.user.id == req.body.id_peminjam){
        models.Pengajuan
        .update(values, options)
        .then(() => {
            models.Pengajuan
            .findOne(options)
            .then(function(result){
                req.flash('success','Data pengajuan berhasil disimpan.');
                res.redirect('/pengajuan');
            });
        });
    }else{
        res.redirect('/pengajuan')
    }
}

exports.approval = function(req, res, next){
    models.Pengajuan
    .findOne({
        where: {
            id: req.params.id
        }
    })
    .then((result) => {
        if(result != null && result.status_booking == 'CREATED'){
            res.render('approval.ejs', {data: result});
        }else{
            return next();
        }
    });
}

exports.approve = function(req, res){
    var req_status = (req.body.approval == 1)? 'APPROVED' : 'REJECTED';
    const values = {
        status_booking: req_status
    };
    
    const options = {
        where: {
            id: req.body.id
        }
    };
    
    models.Pengajuan
    .update(values, options)
    .then(() => {
        models.Pengajuan
        .findOne(options)
        .then(function(result){
            req.flash('success','Berhasil melakukan APPROVE pengajuan.');
            res.redirect('/pengajuan');
        });
    });
}