exports.index = function(req, res){
    res.render('todo.jade', {todolist: req.session.todolist, title: "Express JS"});
};

exports.save = function(req, res){
    if(req.body.newtodo != ''){
        req.session.todolist.push(req.body.newtodo);
    }
    res.redirect('/todo');
}

exports.delete = function(req, res){
    if(req.params.id != ''){
        req.session.todolist.splice(req.params.id, 1);
    }
    res.redirect('/todo');
}